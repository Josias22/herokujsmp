from django import forms
from .models import Member

class JadwalForm(forms.ModelForm):
    class Meta:
        model = Member
        fields = ['matkul', 'dosen', 'sks', 'deskripsi','semester_tahun', 'ruang_kelas']
    
    CHOICES = [
        ('Gasal 2019/2020','Gasal 2019/2020'),
        ('Genap 2019/2020','Genap 2019/2020'),
        ('Gasal 2020/2021','Gasal 2020/2021'),
        ('Genap 2020/2021','Genap 2020/2021')
    ]
    semester_tahun = forms.ChoiceField(choices=CHOICES)