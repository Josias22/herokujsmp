from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    # dilanjutkan ...
    path('', views.index, name='index'),
    path('project', views.project, name = "project"),
    path('home', views.home, name = "home"),
    path('jadwal', views.jadwal, name = "jadwal"),
    path('detail', views.detail, name = "detail"),
    path('delete_items/<int:pk>/', views.deleteTask, name="delete_items"),
    
]