from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Member
from .forms import JadwalForm
# Create your views here.
def index(request):
    return render(request, 'index.html')

def project(request):
    return render(request, 'index2.html')

def home(request):
    return render(request, 'indexhome.html')

def jadwal(request):
    jadwal = Member.objects.all() #ngambil semua data database
    matkul = JadwalForm()
    if request.method == "POST":
        form = JadwalForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('/')
    return render(request, 'jadwal.html' , {'jadwal': jadwal, 'matakuliah' : matkul})

def detail(request):
    detail_jadwal = Member.objects.all()
    matkul = JadwalForm()
    if request.method == "POST":
        form = JadwalForm(request.POST)
        if form.is_valid():
            form.save()
    return render(request, 'detailjadwal.html', {'jadwal': detail_jadwal, 'matakuliah' : matkul})


def deleteTask(request, pk):
    item = Member.objects.get(id=pk)

    if request.method == 'POST':
        item.delete()
        return redirect('/')
        
    context = {'item' : item}
    return render(request, 'delete_items.html', context)
 