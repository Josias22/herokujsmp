from django.db import models

# Create your models here.
class Member(models.Model):
    matkul = models.CharField(max_length=50)
    dosen = models.CharField(max_length=50)
    sks = models.IntegerField()
    deskripsi = models.TextField()
    semester_tahun = models.CharField(max_length=50, null = True)
    ruang_kelas = models.CharField(max_length=20)    

    def __str__(self):
        return self.matkul


